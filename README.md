# Arcanum Tech Test
Thank you for taking the time to read through my work. I enjoyed working on this small project and I hope to have the opportunity to discuss 
my experience with you in person.

## Overview

For this tech test I have implemented a simple recommender system using `Tensorflow` and deployed it as a single-endpoint API using `FastAPI`.
Given the scope of this tech test, my goal is to have a lightweight system that could be put into production with minimal effort.

I have chosen to use the `Apps for Android - Ratings only` dataset to build the model. Details of the model architecture can be found in 
`recommender.ipynb` alongside the model itself and some thoughts about future performance improvements.

## API

The API can be found in the `app` directory. While it is only a single file (`app/main.py`), structuring it as a package leaves it open for 
extension without modifying the `Dockerfile`. The API is called by the `/recommend/{user}` endpoint where `{user}` is the id of the user to 
get recommendations for. An optional parameter `k` can be passed to return a different number of recommendations (the default is 5).

For example:
```
>>> host/recommend/AUI0OLXAB3KKT/?k=2
{
    recommended_items: [
        "B004A9SDD8"
        "B008MULZXG"
    ]
}
```

In terms of the performance, the API takes < 0.01 seconds to compute recommendations locally. I have included a simple `/health` endpoint 
that could be used to monitor the API when deployed. I have also added a generic `CORSMiddleWare` which can be updated based on the needs 
of the actual production system.

## Deployment

For deployment I have created a basic containerisation process. I have used the `python:3.9-slim-bullseye` image, which is a lightweight 
image for python applications. I have chosen this on the basis that the application would be deployed standalone, however if it is 
deployed alongside other python images then a more package-rich base image could save memory overall. I also chose this image as it is
well supported and can easily be deployed anywhere, however once a deployment platform is chosen a more specific image could be a better 
choice, for example using the official AWS ECR image if deploying on that platform.

For simplicity's sake I have included the model in the container, however in an actual production system it would be a good idea to store 
models elsewhere and load them at run time. This would allow for the model to be updated without rebuilding the API container. A simple 
example using AWS would be saving models to s3 with a version tag, deploying the API on Elastic Container Registry and including some 
sort of automated model re-loading in our application, for example checking for new versions on a schedule.

I decided not to add a development stage to the container build. This is due to being unclear on the development requirements; do we simply 
want an environment for building and testing the API, in which case the requirements are quite similar, or for developing and training the
recommender system itself, where the requirements are considerably different. In the latter case we would likely want to use an image optimised 
for GPU-training (CUDA) whereas the API is performant using only CPU for inference.

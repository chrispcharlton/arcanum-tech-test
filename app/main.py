import numpy as np
import tensorflow as tf
from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware


app = FastAPI()

# Generic middleware, update based on actual product requirements
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=False,
    allow_methods=["*"],
    allow_headers=["*"],
)

model = tf.saved_model.load("model")


@app.get("/health")
async def health():
    """simple health check for monitoring."""
    return {
        "status": "OK"
    }


@app.get("/recommend/{user}")
async def recommend(user: str, k: int = 5):
    """Recommend items for a given user.

    Arguments:
        user: str, a valid user id
        k: an integer between 0 and 10 that determines the number of recommendations returned.

    Returns:
        An array of the top k items for the given user. If k is not given then k=5 will be used.
    """
    # clamp k to range 0, 10 to avoid awkward behaviour (i.e. k = -1 would return the 10th-ranked recommendation)
    k = max(0, min(k, 10))
    _, items = model(
        {
            "users": np.array([user]),
            "rating": np.array([5])
        }
    )
    recommended_items = list(np.array(items[0]))
    return {
        "recommended_items": recommended_items[:k]
    }

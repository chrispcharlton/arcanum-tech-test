{
 "cells": [
  {
   "cell_type": "markdown",
   "source": [
    "# Building a simple recommendation system\n",
    "\n",
    "I have chosen to implement a simple item recommender system. This system will take a user and a rating as input and retrieve a list of top items. If we call this model with a high rating (5) we can think of it as answering the question \"which items is this user likely to rate highly?\"; we can then present these items as recommendations to the user.\n",
    "\n",
    "I am using a two-part query and candidate model architecture. The query and candidate \"sub-models\" learn embeddings of the input data and candidate items respectively, which are combined to give an overall score for each query-candidate pair. This score is then used to rank candidate options for queries, and the models are optimised to produce rankings where positive candidates (those that have been observed for a particular query) are ranked highly by the model. This is a relatively common approach to implementing a recommender system on this kind of data. I also chose this as the `tensorflow-recommenders` library has some convenient classes for constructing this kind of model, which made it relatively simple to train and deploy.\n",
    "\n",
    "Given our data contains only positive examples (a user rated an item), we use the _top-k accuracy_ metric for evaluation. This measures the proportion of predictions of size `k` that contained a positive example (item rated by that user). There are a number of potential issues with this evaluation metric however it is simple to implement and interpret, which seems suitable for this exercise. While our model takes rating as an input, we use it purely as an input feature and not to inform the relevance or suitability of a recommendation when evaluating the model."
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "# Setup\n",
    "We start by importing required packages. We are going to make use of `tensorflow-recommenders` to quickly build a simple model."
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": true,
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import numpy as np\n",
    "import tensorflow as tf\n",
    "import tensorflow_recommenders as tfrs\n",
    "\n",
    "# Hide TF warnings to reduce noise in notebook output\n",
    "tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)"
   ]
  },
  {
   "cell_type": "markdown",
   "source": [
    "Here we set model parameters. These can be tuned to optimise training time and model performance, however I have chosen some relatively standard values as a starting point."
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "outputs": [],
   "source": [
    "batch_size = 2048\n",
    "n_epochs = 3\n",
    "embedding_units = 32\n",
    "learning_rate = 0.1"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "Next we load our data set. I have chosen the `ratings_Apps_for_Android` data set as it had a relatively high density of users with 5 or more reviews. This implies the `user x product` matrix is less sparse than in other categories and thus our model may be easier to train. You might prefer to limit the number of rows loaded to run the workbook quickly as a \"proof of concept\" rather than training a model on the entire data set.\n",
    "\n",
    "We load data from the downloaded csv file and package the features we will use into a TensorFlow `Dataset`."
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "outputs": [
    {
     "data": {
      "text/plain": "                   user        item  rating   timestamp\n1986501  A1A7PAN1FKFD7N  B00CWY76CC     2.0  1012003200\n144066   A34S9U8RPU2BO2  B004SBS8LA     5.0  1043452800\n514108    AIE411RFIT7GO  B0064X7B4A     1.0  1119484800\n1346950  A32NTV93B15MYW  B00992CF6W     5.0  1126483200\n488976   A1BQ8UOASGLQSP  B0063IH60K     5.0  1134777600",
      "text/html": "<div>\n<style scoped>\n    .dataframe tbody tr th:only-of-type {\n        vertical-align: middle;\n    }\n\n    .dataframe tbody tr th {\n        vertical-align: top;\n    }\n\n    .dataframe thead th {\n        text-align: right;\n    }\n</style>\n<table border=\"1\" class=\"dataframe\">\n  <thead>\n    <tr style=\"text-align: right;\">\n      <th></th>\n      <th>user</th>\n      <th>item</th>\n      <th>rating</th>\n      <th>timestamp</th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr>\n      <th>1986501</th>\n      <td>A1A7PAN1FKFD7N</td>\n      <td>B00CWY76CC</td>\n      <td>2.0</td>\n      <td>1012003200</td>\n    </tr>\n    <tr>\n      <th>144066</th>\n      <td>A34S9U8RPU2BO2</td>\n      <td>B004SBS8LA</td>\n      <td>5.0</td>\n      <td>1043452800</td>\n    </tr>\n    <tr>\n      <th>514108</th>\n      <td>AIE411RFIT7GO</td>\n      <td>B0064X7B4A</td>\n      <td>1.0</td>\n      <td>1119484800</td>\n    </tr>\n    <tr>\n      <th>1346950</th>\n      <td>A32NTV93B15MYW</td>\n      <td>B00992CF6W</td>\n      <td>5.0</td>\n      <td>1126483200</td>\n    </tr>\n    <tr>\n      <th>488976</th>\n      <td>A1BQ8UOASGLQSP</td>\n      <td>B0063IH60K</td>\n      <td>5.0</td>\n      <td>1134777600</td>\n    </tr>\n  </tbody>\n</table>\n</div>"
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "data = pd.read_csv(\"ratings_Apps_for_Android.csv\", header=None)\n",
    "data.columns = [\"user\", \"item\", \"rating\", \"timestamp\"]\n",
    "data = data.sort_values(\"timestamp\")\n",
    "data.head()"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "2022-06-08 16:12:18.840837: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:936] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero\n",
      "2022-06-08 16:12:18.841182: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:936] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero\n",
      "2022-06-08 16:12:18.865105: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:936] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero\n",
      "2022-06-08 16:12:18.865293: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:936] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero\n",
      "2022-06-08 16:12:18.865457: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:936] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero\n",
      "2022-06-08 16:12:18.865611: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:936] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero\n",
      "2022-06-08 16:12:18.866220: I tensorflow/core/platform/cpu_feature_guard.cc:151] This TensorFlow binary is optimized with oneAPI Deep Neural Network Library (oneDNN) to use the following CPU instructions in performance-critical operations:  AVX2 FMA\n",
      "To enable them in other operations, rebuild TensorFlow with the appropriate compiler flags.\n",
      "2022-06-08 16:12:18.967756: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:936] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero\n",
      "2022-06-08 16:12:18.967936: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:936] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero\n",
      "2022-06-08 16:12:18.968082: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:936] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero\n",
      "2022-06-08 16:12:18.968212: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:936] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero\n",
      "2022-06-08 16:12:18.968342: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:936] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero\n",
      "2022-06-08 16:12:18.968471: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:936] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero\n",
      "2022-06-08 16:12:19.629853: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:936] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero\n",
      "2022-06-08 16:12:19.630074: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:936] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero\n",
      "2022-06-08 16:12:19.630247: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:936] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero\n",
      "2022-06-08 16:12:19.630410: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:936] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero\n",
      "2022-06-08 16:12:19.630566: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:936] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero\n",
      "2022-06-08 16:12:19.630727: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1525] Created device /job:localhost/replica:0/task:0/device:GPU:0 with 3982 MB memory:  -> device: 0, name: NVIDIA GeForce RTX 3070, pci bus id: 0000:0b:00.0, compute capability: 8.6\n",
      "2022-06-08 16:12:19.631024: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:936] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero\n",
      "2022-06-08 16:12:19.631173: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1525] Created device /job:localhost/replica:0/task:0/device:GPU:1 with 5807 MB memory:  -> device: 1, name: NVIDIA GeForce RTX 3070, pci bus id: 0000:0c:00.0, compute capability: 8.6\n"
     ]
    }
   ],
   "source": [
    "dataset = tf.data.Dataset.from_tensor_slices(\n",
    "    (\n",
    "        {\n",
    "            \"users\": data.user.to_list(),\n",
    "            \"items\": data.item.to_list(),\n",
    "            \"rating\": data.rating.to_list()\n",
    "        }\n",
    "    )\n",
    ").cache()"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "# Building the Model\n",
    "The Query model is implemented to use **user id** and **rating** as input features."
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "outputs": [],
   "source": [
    "class QueryModel(tf.keras.Model):\n",
    "    def __init__(self, unique_users):\n",
    "        super().__init__()\n",
    "        self.user_embedding = tf.keras.Sequential(\n",
    "            [\n",
    "                tf.keras.layers.StringLookup(\n",
    "                    vocabulary=unique_users, mask_token=None\n",
    "                ),\n",
    "                tf.keras.layers.Embedding(\n",
    "                    len(unique_users) + 1,\n",
    "                    embedding_units\n",
    "                )\n",
    "            ]\n",
    "        )\n",
    "        self.rating_embedding = tf.keras.layers.Embedding(6, embedding_units)\n",
    "\n",
    "    def call(self, inputs):\n",
    "        return tf.concat(\n",
    "            [\n",
    "                self.user_embedding(inputs[\"users\"]),\n",
    "                self.rating_embedding(inputs[\"rating\"])\n",
    "            ], axis=1\n",
    "        )\n",
    "\n",
    "unique_users = data.user.unique()\n",
    "query_model = QueryModel(unique_users)"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "The candidate model provides item embeddings."
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "outputs": [],
   "source": [
    "unique_items = np.unique(data.item.to_list())\n",
    "candidate_model = tf.keras.Sequential(\n",
    "    [\n",
    "        tf.keras.layers.StringLookup(\n",
    "            vocabulary=unique_items, mask_token=None\n",
    "        ),\n",
    "        tf.keras.layers.Embedding(\n",
    "            len(unique_items) + 1,\n",
    "            embedding_units\n",
    "        )\n",
    "    ]\n",
    ")"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "We use the `FactorizedTopK` metric and `Retrieval` task from `tensorflow-recommenders`."
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "outputs": [],
   "source": [
    "metrics = tfrs.metrics.FactorizedTopK(\n",
    "  candidates=tf.data.Dataset.from_tensor_slices(unique_items).batch(128).map(candidate_model),\n",
    ")\n",
    "\n",
    "task = tfrs.tasks.Retrieval(\n",
    "  metrics=metrics\n",
    ")"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "The final model combines both sets of embeddings."
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "outputs": [],
   "source": [
    "class Recommender(tfrs.Model):\n",
    "    def __init__(self, query_model, candidate__model):\n",
    "        super().__init__()\n",
    "        self.query_model = tf.keras.Sequential(\n",
    "            [\n",
    "                query_model,\n",
    "                tf.keras.layers.Dense(embedding_units)\n",
    "            ]\n",
    "        )\n",
    "        self.item_model = tf.keras.Sequential(\n",
    "            [\n",
    "                candidate__model,\n",
    "                tf.keras.layers.Dense(embedding_units)\n",
    "            ]\n",
    "        )\n",
    "        self.task = task\n",
    "\n",
    "    def compute_loss(self, features, training=False) -> tf.Tensor:\n",
    "        query_embeddings = self.query_model({\"users\": features[\"users\"], \"rating\": features[\"rating\"]})\n",
    "        item_embeddings = self.item_model(features[\"items\"])\n",
    "        return self.task(query_embeddings, item_embeddings)"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "# Training the model\n",
    "Now we can initialise and train our model."
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "outputs": [],
   "source": [
    "model = Recommender(query_model, candidate_model)\n",
    "model.compile(optimizer=tf.keras.optimizers.Adagrad(learning_rate=learning_rate))"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "Here we do a simple 80/20 test/train split, splitting the data randomly (i.e shuffling the dataset first). An alternative approach in a production system might be something like selecting recent examples as the test data which could allow us to better detect drift and trigger retraining. However this might have unwanted effects such as our system not recommending recently released items (which we may in fact prefer to bias _towards_) and so I have gone for the simple approach here."
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "outputs": [],
   "source": [
    "shuffled = dataset.shuffle(1000000, seed=1337, reshuffle_each_iteration=False)\n",
    "split_point = int(len(dataset) * 0.8)\n",
    "train = shuffled.take(split_point)\n",
    "test = shuffled.skip(split_point).take(len(shuffled) - split_point)\n",
    "# Shuffle training data on each iteration\n",
    "cached_train = train.shuffle(1000000).batch(batch_size).cache()\n",
    "cached_test = test.batch(batch_size).cache()"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Epoch 1/3\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "2022-06-08 16:12:33.354464: I tensorflow/stream_executor/cuda/cuda_blas.cc:1786] TensorFloat-32 will be used for the matrix multiplication. This will only be logged once.\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1031/1031 [==============================] - 1296s 1s/step - factorized_top_k/top_1_categorical_accuracy: 0.4628 - factorized_top_k/top_5_categorical_accuracy: 0.5036 - factorized_top_k/top_10_categorical_accuracy: 0.5224 - factorized_top_k/top_50_categorical_accuracy: 0.5724 - factorized_top_k/top_100_categorical_accuracy: 0.5963 - loss: 15127.1209 - regularization_loss: 0.0000e+00 - total_loss: 15127.1209\n",
      "Epoch 2/3\n",
      "1031/1031 [==============================] - 1304s 1s/step - factorized_top_k/top_1_categorical_accuracy: 0.9180 - factorized_top_k/top_5_categorical_accuracy: 0.9308 - factorized_top_k/top_10_categorical_accuracy: 0.9359 - factorized_top_k/top_50_categorical_accuracy: 0.9479 - factorized_top_k/top_100_categorical_accuracy: 0.9531 - loss: 12388.3844 - regularization_loss: 0.0000e+00 - total_loss: 12388.3844\n",
      "Epoch 3/3\n",
      "1031/1031 [==============================] - 1302s 1s/step - factorized_top_k/top_1_categorical_accuracy: 0.9970 - factorized_top_k/top_5_categorical_accuracy: 0.9975 - factorized_top_k/top_10_categorical_accuracy: 0.9977 - factorized_top_k/top_50_categorical_accuracy: 0.9981 - factorized_top_k/top_100_categorical_accuracy: 0.9982 - loss: 8404.0726 - regularization_loss: 0.0000e+00 - total_loss: 8404.0726\n",
      "258/258 [==============================] - 320s 1s/step - factorized_top_k/top_1_categorical_accuracy: 0.9945 - factorized_top_k/top_5_categorical_accuracy: 0.9954 - factorized_top_k/top_10_categorical_accuracy: 0.9958 - factorized_top_k/top_50_categorical_accuracy: 0.9965 - factorized_top_k/top_100_categorical_accuracy: 0.9968 - loss: 18179.5484 - regularization_loss: 0.0000e+00 - total_loss: 18179.5484\n"
     ]
    },
    {
     "data": {
      "text/plain": "{'factorized_top_k/top_1_categorical_accuracy': 0.9944829344749451,\n 'factorized_top_k/top_5_categorical_accuracy': 0.9954267740249634,\n 'factorized_top_k/top_10_categorical_accuracy': 0.9957754611968994,\n 'factorized_top_k/top_50_categorical_accuracy': 0.9964501857757568,\n 'factorized_top_k/top_100_categorical_accuracy': 0.9967913627624512,\n 'loss': 10848.3818359375,\n 'regularization_loss': 0,\n 'total_loss': 10848.3818359375}"
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "model.fit(cached_train, epochs=n_epochs)\n",
    "model.evaluate(cached_test, return_dict=True)"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "Finally we create a `BruteForce` layer on top of our model to implement brute-force retrieval using our trained model. This is an easy way to package our models for inference. The drawback of the brute-force approach is that it scales poorly as the number of candidates increases. In this case we might prefer to use an approximate solution like a scaNN method. However after briefly testing the scaNN approach I found that brute-force was faster for our data set so we will use it here."
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "2022-06-08 17:22:52.468141: W tensorflow/python/util/util.cc:368] Sets are not currently considered sequences, but this may change in the future, so consider avoiding using them.\n",
      "WARNING:absl:Found untraced functions such as query_with_exclusions while saving (showing 1 of 1). These functions will not be directly callable after loading.\n"
     ]
    }
   ],
   "source": [
    "brute_force = tfrs.layers.factorized_top_k.BruteForce(model.query_model)\n",
    "brute_force.index_from_dataset(\n",
    "    tf.data.Dataset.zip((tf.data.Dataset.from_tensor_slices(unique_items).batch(128), tf.data.Dataset.from_tensor_slices(unique_items).batch(128).map(model.item_model)))\n",
    ")\n",
    "# we need to call the model before we can save\n",
    "_, items = brute_force(\n",
    "        {\n",
    "            \"users\": np.array([\"AUI0OLXAB3KKT\"]),\n",
    "            \"rating\": np.array([5])\n",
    "        }\n",
    "    )\n",
    "tf.saved_model.save(brute_force, \"model\")"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "outputs": [],
   "source": [
    "loaded = tf.saved_model.load(\"model\")\n",
    "_, items = loaded(\n",
    "        {\n",
    "            \"users\": np.array([\"AUI0OLXAB3KKT\"]),\n",
    "            \"rating\": np.array([5])\n",
    "        }\n",
    ")"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "# Model Performance\n",
    "\n",
    "Overall our model performs well, with a high top-5/top-10 accuracy over our test set. This means that our model ranks positive (observed in the data) candidates highly and thus should rank similar candidates highly. One issue with this is that our model may provide a lot of redundant recommendations (i.e. items the user has already rated). If our goal is to suggest the next item a user might purchase then these recommendations may not be useful.\n",
    "\n",
    "A better model evaluation might be using precision/recall measures with some means of rating the relevance of items. For example, measuring the proportion of highly-rated items that were retrieved. This has it's own issues however where we may end up with suggestions that are too homogenous, biasing towards overall user ratings more than similar users. Our model could also be biased toward low density items that are highly rated, but may not be relevant to the majority of users. If we wanted to use this kind of metric we would need to take care to carefully design a relevance metric so as not to improperly bias our model.\n",
    "\n",
    "An \"easy\" way to improve our model would be the obvious \"use more data\". Recommender systems in particular tend to suffer from very sparse data (the `user x item` matrix). A key area of improvement would be adding data that helps the model to generalise among items and users. For example providing a `category` such as \"productivity\" or \"game\" for each rated app would let the model learn which subset of apps a user or group of users often purchases, rather than being limited to learning this relationship for only specific apps. Alternatively we could provide user features such as average spend or purchases in other item categories. This kind of contextual information is particularly important if we want our system to quickly adapt to new data. The downside of this is that we would need to implement a more complex model architecture.\n",
    "\n",
    "Finally we could consider a different model architecture altogether. I went with this architecture mostly because it was relatively simple and quick to implement. It has been suggested that Recurrent Nueral Networks (RNNs) offer similar or better performance than traditional recommender systems approaches by predicting the next item a user will purchase, which is an alternative way of framing the problem (as opposed to \"what items is the user likely to rate highly?\"). There has also been work into using two-way attention to increase the training data by masking values from a customers purchase history and predicting those, rather than just using the most recent purchase as a training example. One issue with RNNs is that they may not work well on a very sparse data set where there are a lot of users with very short (or no) purchase history. In this case we might need to provide the model with more user features to support generalisation."
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
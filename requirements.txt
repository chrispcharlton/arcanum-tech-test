fastapi==0.78.0
numpy==1.22.4
pydantic==1.9.1
starlette==0.19.1
tensorflow-cpu==2.9.0
uvicorn==0.17.6
